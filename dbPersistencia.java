package Modelo;
import java.util.ArrayList;
public interface dbPersistencia {
    public void insertar(Object objeto) throws Exception;
    public ArrayList listar() throws Exception;
    public Object buscar(String nombre) throws Exception;
    public boolean isExiste(String nombre) throws Exception;
    public boolean isCoincide(String nombre, String contraseña) throws Exception;
    
}
